#!/sbin/sh
# --------------------------------
# TGP INSTALLER v11.2.2
# Created by @djb77
#
# Credit also goes to @lyapota
# @farovitus, @Morogoku, @dwander,
# @Chainfire, @osm0sis, @Tkkg1994,
# @ambasadii, , @~clumsy~ @mwilky,
# and @topjohnwu for code snippets.
# --------------------------------

# Read option number from updater-script
OPTION=$1

# Block location
BLOCK=/dev/block/platform/11120000.ufs/by-name

# Variables
TGPTEMP=/tmp/tgptemp
AROMA=/tmp/aroma
TGP=/data/media/0/TGP
CONFIG=$TGP/config
BUILDPROP=/system/build.prop
PROPDEFAULT=/system/etc/prop.default
VENDORBUILDPROP=/vendor/build.prop
VENDORDEFAULTPROP=/vendor/default.prop
FLOATINGFEATURE=/system/etc/floating_feature.xml
KERNEL_REMOVE="init.services.rc init.PRIME-Kernel.rc init.spectrum.sh init.spectrum.rc init.primal.rc init.noto.rc kernelinit.sh wakelock.sh super.sh cortexbrain-tune.sh spectrum.sh kernelinit.sh spa init_d.sh initd.sh moro-init.sh sysinit.sh tgpkernel.sh noto.sh"

if [ $OPTION == "setup" ]; then
	## Set Permissions
	chmod 755 $AROMA/adb
	chmod 755 $AROMA/adb.bin
	chmod 755 $AROMA/fastboot
	chmod 755 $AROMA/busybox
	chmod 755 $AROMA/fstrim
	chmod 755 $AROMA/tar
	chmod 755 $AROMA/zip
	chmod 755 $AROMA/tgp.sh
	exit 10
fi

if [ $OPTION == "config_check" ]; then
	## Config Check
	# If config backup is present, alert installer
	mount $BLOCK/USERDATA /data
	if [ -e $CONFIG/tgp-backup.prop ]; then
		echo "install=1" > $AROMA/backup.prop
	fi
	exit 10
fi

if [ $OPTION == "config_backup" ]; then
	## Backup Config
	# Check if TGP folder exists on Internal Memory, if not, it is created
	if [ ! -d $TGP ]; then
		mkdir $TGP
		chmod 777 $TGP
	fi
	# Check if config folder exists, if it does, delete it 
	if [ -d $CONFIG-backup ]; then
		rm -rf $CONFIG-backup
	fi
	# Check if config folder exists, if it does, ranme to backup
	if [ -d $CONFIG ]; then
		mv -f $CONFIG $CONFIG-backup
	fi
	# Check if config folder exists, if not, it is created
	if [ ! -d $CONFIG ]; then
		mkdir $CONFIG
		chmod 777 $CONFIG
	fi
	# Copy files from $AROMA to backup location
	cp -f $AROMA/* $CONFIG
	# Delete any files from backup that are not .prop files
	find $CONFIG -type f ! -iname "*.prop" -delete
	# Remove unwanted .prop files from the backup
	cd $CONFIG
	[ -f "$CONFIG/g960f.prop" ] && rm -f $CONFIG/g960f.prop
	[ -f "$CONFIG/g960fd.prop" ] && rm -f $CONFIG/g960fd.prop
	[ -f "$CONFIG/g960n.prop" ] && rm -f $CONFIG/g960n.prop
	[ -f "$CONFIG/g960x.prop" ] && rm -f $CONFIG/g960x.prop
	[ -f "$CONFIG/g965f.prop" ] && rm -f $CONFIG/g965f.prop
	[ -f "$CONFIG/g965fd.prop" ] && rm -f $CONFIG/g965fd.prop
	[ -f "$CONFIG/g965n.prop" ] && rm -f $CONFIG/g965n.prop
	[ -f "$CONFIG/g965x.prop" ] && rm -f $CONFIG/g965x.prop
	for delete_prop in *.prop 
	do
		if grep "item" "$delete_prop"; then
			rm -f $delete_prop
		fi
		if grep "install=0" "$delete_prop"; then
			rm -f $delete_prop
		fi 
	done
	exit 10
fi

if [ $OPTION == "config_restore" ]; then
	## Restore Config
	# Copy backed up config files to $AROMA
	cp -f $CONFIG/* $AROMA
	exit 10
fi

if [ $OPTION == "wipe_magisk" ]; then
	## Wipe old Magisk / SuperSU Installs (@mwilky)
	mount /cache
	rm -rf /cache/magisk.log /cache/last_magisk.log /cache/magiskhide.log \
		 /cache/.disable_magisk /cache/magisk /cache/magisk_merge /cache/magisk_mount \
		 /cache/unblock /cache/magisk_uninstaller.sh /data/Magisk.apk /data/magisk.apk \
		 /data/magisk.img /data/magisk_merge.img /data/busybox /data/magisk /data/custom_ramdisk_patch.sh 2>/dev/null
	rm -rf /cache/.supersu /cache/su.img /cache/SuperSU.apk \
	     /data/.supersu /data/stock_boot_*.img.gz /data/su.img \
	     /data/SuperSU.apk /data/app/eu.chainfire.supersu* \
	     /data/data/eu.chainfire.supersu /data/supersu /supersu
	exit 10
fi

if [ $OPTION == "check_g960f" ]; then
	## Variant Check for G960F
	echo "install=1" > $AROMA/g960f.prop
	echo "install=1" > $AROMA/g960x.prop
	exit 10
fi

if [ $OPTION == "check_g960fd" ]; then
	## Variant Check for G960FD
	echo "install=0" > $AROMA/g960f.prop
	echo "install=1" > $AROMA/g960fd.prop
	echo "install=1" > $AROMA/g960x.prop
	exit 10
fi

if [ $OPTION == "check_g960n" ]; then
	## Variant Check for G960N
	echo "install=1" > $AROMA/g960n.prop
	echo "install=1" > $AROMA/g960x.prop
	exit 10
fi

if [ $OPTION == "check_g965f" ]; then
	## Variant Check for G965F
	echo "install=1" > $AROMA/g965f.prop
	echo "install=1" > $AROMA/g965x.prop
	exit 10
fi

if [ $OPTION == "check_g965fd" ]; then
	## Variant Check for G965FD
	echo "install=0" > $AROMA/g965f.prop
	echo "install=1" > $AROMA/g965fd.prop
	echo "install=1" > $AROMA/g965x.prop
	exit 10
fi

if [ $OPTION == "check_g965f" ]; then
	## Variant Check for G965N
	echo "install=1" > $AROMA/g965n.prop
	echo "install=1" > $AROMA/g965x.prop
	exit 10
fi

if [ $OPTION == "backup_efs" ]; then	
	## Backup EFS
	mount $BLOCK/EFS /efs
	if [ ! -d /data/media/0/TGP ];then
	mkdir /data/media/0/TGP
	chmod -R 777 /data/media/0/TGP
	fi
	if [ -e /data/media/0/TGP/efs.img.bak ]; then
	rm -f /data/media/0/TGP/efs.img.bak 
	fi
	if [ -e /data/media/0/TGP/efs.img ]; then
	cp -f /data/media/0/TGP/efs.img /data/media/0/TGP/efs.img.bak
	rm -f /data/media/0/TGP/efs.img
	fi
	dd if=$BLOCK/EFS of=/data/media/0/TGP/efs.img bs=4096
	exit 10
fi

if [ $OPTION == "wipe" ]; then
	## Full Wipe
	find /data -maxdepth 1 -type d ! -path "/data" ! -path "/data/media" | xargs rm -rf
	exit 10
fi

if [ $OPTION == "system_patch" ]; then
	## System Patches
	# Patch prop.default
	sed -i '/ro.debuggable/a ro.config.tima=0\nro.security.vaultkeeper.feature=0\nwlan.wfd.hdcp=disable\nro.securestorage.support=false' $PROPDEFAULT
	# Remove RMM State Lock
	rm -rf /system/priv-app/Rlc
	# Remove Security Log Agent
	rm -rf /system/app/SecurityLogAgent
	# Remove init.d and addon.d Placeholders
	rm -f /system/addon.d/placeholder
	rm -f /system/etc/init.d/placeholder
	# Delete Wakelock.sh 
	rm -f /magisk/phh/su.d/wakelock*
	rm -f /su/su.d/wakelock*
	rm -f /system/su.d/wakelock*
	rm -f /system/etc/init.d/wakelock*
	# build.prop Mods
	sed -i -- 's/ro.config.tima=1/ro.config.tima=0/g' $BUILDPROP
	echo "# Screen mirror fix" >> $BUILDPROP
	echo "wlan.wfd.hdcp=disable" >> $BUILDPROP
	echo "# Fix Fingerprint Unlock" >> $BUILDPROP
	echo "fingerprint.unlock=1" >> $BUILDPROP
	echo "# Fix Mobile Hotspot" >> $BUILDPROP
	echo "net.tethering.noprovisioning=true" >> $BUILDPROP
	echo "# Fix FPS for Boot/Shutdown Animations" >> $BUILDPROP
	echo "boot.fps=25" >> $BUILDPROP
	echo "shutdown.fps=25" >> $BUILDPROP
	# Variant Fixes (@mwilky)
	if grep -q install=1 $AROMA/g960x.prop; then
		sed -i -- 's/G965/G960/g' $BUILDPROP $PROPDEFAULT /system/info.extra
		sed -i -- 's/star2lte/starlte/g' $BUILDPROP $PROPDEFAULT /system/etc/palm_config.xml
		sed -i -- 's/1422,0/1317,0/g' $FLOATINGFEATURE
		sed -i -- 's/S9+/S9/g' $FLOATINGFEATURE
		sed -i -- 's/A3LSMG965F/A3LSMG960F/g' $FLOATINGFEATURE
		sed -i -- 's/CONFIG_DEFAULT_FONT_SIZE>3/CONFIG_DEFAULT_FONT_SIZE>2/g' $FLOATINGFEATURE
	fi
	exit 10
	# Fix sepolicy for Deodexed ROMs
	sed -i -- 's/(allow zygote dalvikcache_data_file (file (ioctl read write create getattr setattr lock append unlink rename open)))/(allow zygote dalvikcache_data_file (file (ioctl read write create getattr setattr lock append unlink rename open execute)))/g' /system/etc/selinux/plat_sepolicy.cil
fi

if [ $OPTION == "odm_patch" ]; then
	## ODM Patches
	# Variant Fixes (@mwilky)
	if grep -q install=1 $AROMA/g960x.prop; then
		sed -i -- 's/G965/G960/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965F/SM-G960F/g'
	fi
	exit 10
fi

if [ $OPTION == "vendor_patch" ]; then
	## Vendor Patches
	# Remove unwanted file from /vendor/app/mcRegistry
	rm -f /vendor/app/mcRegistry/ffffffffd00000000000000000000062.tlbin
	# build.prop Mods
	sed -i -- 's/ro.security.vaultkeeper.feature=1/ro.security.vaultkeeper.feature=0/g' $VENDORBUILDPROP
	sed -i -- 's/o.knox.enhance.zygote.aslr=1/o.knox.enhance.zygote.aslr=0/g' $VENDORBUILDPROP
	sed -i -- 's/ro.frp.pst=\/dev\/block\/persistent/ro.frp.pst=/g' $VENDORBUILDPROP
	# Fix sepolicy for Deodexed ROMs
	echo "(allow zygote_26_0 dalvikcache_data_file_26_0 (file (execute)))" >> /vendor/etc/selinux/nonplat_sepolicy.cil
	exit 10
fi

if [ $OPTION == "csc_setup" ]; then
	# Find CSC ID and dump to a /tmp file
	if [ ! -e "/efs/imei/mps_code.dat" ]; then
		mount /efs
	fi	
	csc_id=`cat /efs/imei/mps_code.dat`
	echo "$csc_id" > /tmp/csc_id.prop	
	echo "Your active csc is $csc_id"	
	if [[ -d "/odm/omc/$csc_id" || -d "/odm/omc/single/$csc_id" ]]; then
		echo "CSC code $csc_id found in rom zip"
	else
		echo "CSC not found in rom zip, will use XEU"
	fi
	exit 10
fi

if [ $OPTION == "csc_mod" ]; then
	# Feature XML Edit in directory (@~clumsy~, modified by @djb77)
	csc_id=`cat /tmp/csc_id.prop`
	pattern=cscfeature.xml
	feature=$2
	value=$3
	dir=/odm/omc/$csc_id/conf
	if [ -d $dir ]; then 
		for file in $(find $dir -name $pattern)
		do
			lineNumber=0
			lineNumber=`sed -n "/<${feature}>.*<\/${feature}>/=" $file`
			if [ $lineNumber > 0 ] ; then
				echo "Found feature $feature in line $lineNumber and changing it to ${value}"
				sed -i "${lineNumber} c<${feature}>${value}<\/${feature}>" $file
			else
				echo "Adding feature $feature to the feature set and changing it to ${value}"
				sed -i "/<\/FeatureSet>/i <${feature}>${value}<\/${feature}>" $file
			fi
		done
	fi
	dir=/odm/omc/single/$csc_id/conf
	if [ -d $dir ]; then 
		for file in $(find $dir -name $pattern)
		do
			lineNumber=0
			lineNumber=`sed -n "/<${feature}>.*<\/${feature}>/=" $file`
			if [ $lineNumber > 0 ] ; then
				echo "Found feature $feature in line $lineNumber and changing it to ${value}"
				sed -i "${lineNumber} c<${feature}>${value}<\/${feature}>" $file
			else
				echo "Adding feature $feature to the feature set and changing it to ${value}"
				sed -i "/<\/FeatureSet>/i <${feature}>${value}<\/${feature}>" $file
			fi
		done
	fi
	exit 10
fi

if [ $OPTION == "copy_aod_images" ]; then
	## Copy AOD Home button image over to temp location for APK Patcher
	image=$2
	if [ ! -d /tmp/apk_patch ]; then
		mkdir /tmp/apk_patch
		chmod 644 /tmp/apk_patch
	fi
	cp -rf /tmp/aod_images/$image/* /tmp/apk_patch
	exit 10
fi

if [ $OPTION == "copy_navbar_images" ]; then
	## Copy Navigation Bar images over to temp location for APK Patcher
	image=$2
	if [ ! -d /tmp/apk_patch ]; then
		mkdir /tmp/apk_patch
		chmod 644 /tmp/apk_patch
	fi
	cp -rf /tmp/navbar_images/$image/* /tmp/apk_patch
	exit 10
fi

if [ $OPTION == "apk_patch" ]; then
	## APK Patcher
	# APK Patching Script
	FUNC_PATCH()
	{
	echo "Patching $APKName"
	if [ $APKName == "framework-res" ]; then
		cp -f $APKLocation/$APKName.apk $APKPatch.zip
	else
		cp -f $APKLocation/$APKName/$APKName.apk $APKPatch.zip
	fi
	cd $APKPatch
	$AROMA/zip -r -9 $APKPatch.zip *
	[ -f resources.arsc ] && $AROMA/zip -r -0 $APKPatch.zip resources.arsc
	rm -rf $APKLocation/$APKName/$APKName.apk
	if [ $APKName == "framework-res" ]; then
		cp -f $APKPatch.zip $APKLocation/$APKName.apk
	else
		cp -f $APKPatch.zip $APKLocation/$APKName/$APKName.apk
	fi
	}
	# Patch framework.res.apk
	APKLocation=/system/framework
	APKName=framework-res
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch AODService_v30.apk
	APKLocation=/system/priv-app
	APKName=AODService_v30
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch SystemUI.apk
	APKLocation=/system/priv-app
	APKName=SystemUI
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch ThemeCenter.apk
	APKLocation=/system/priv-app
	APKName=ThemeCenter
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch wallpaper-res.apk
	APKLocation=/system/priv-app
	APKName=wallpaper-res
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	exit 10
fi

if [ $OPTION == "kernel_flash" ]; then
	## Set Kernel
	image=$2
	## Flash Kernel (@dwander)
	# Clean up old kernels
	for i in $KERNEL_REMOVE; do
		if test -f $i; then
			[ -f $1 ] && rm -f $i
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/$i/d" init.rc 
			sed -i "/$i/d" init.samsungexynos8910.rc 
		fi
		if test -f sbin/$i; then
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/sbin\/$i/d" init.rc 
			sed -i "/sbin\/$i/d" init.samsungexynos8910.rc 
		fi
	done
	for i in $(ls ./res); do
		test $i != "images" && rm -R ./res/$i
	done
	[ -f /system/bin/uci ] && rm -f /system/bin/uci
	[ -f /system/xbin/uci ] && rm -f /system/xbin/uci
	# Flash new Image
	if grep -q install=1 $AROMA/g960x.prop; then
		$AROMA/busybox dd if=$TGPTEMP/kernels/$image-960.img of=$BLOCK/BOOT
	fi
	if grep -q install=1 $AROMA/g965x.prop; then
		$AROMA/busybox dd if=$TGPTEMP/kernels/$image-965.img of=$BLOCK/BOOT
	fi
	sync
	exit 10
fi

if [ $OPTION == "splash_flash" ]; then
	## Custom Splash Screen (@Tkkg1994)
	cd /tmp/splash
	mkdir /tmp/splashtmp
	cd /tmp/splashtmp
	$AROMA/tar -xf $BLOCK/UP_PARAM
	cp /tmp/splash/logo.jpg .
	chown root:root *
	chmod 444 logo.jpg
	touch *
	$AROMA/tar -pcvf ../new.tar *
	cd ..
	cat new.tar > $BLOCK/UP_PARAM
	cd /
	rm -rf /tmp/splashtmp
	rm -f /tmp/new.tar
	sync
	exit 10
fi

if [ $OPTION == "adb" ]; then
	## Install ADB
	rm -f /system/xbin/adb /system/xbin/adb.bin /system/xbin/fastboot
	cp -f $AROMA/adb /system/xbin/adb
	cp -f $AROMA/adb.bin /system/xbin/adb.bin
	cp -f $AROMA/fastboot /system/xbin/fastboot
	chown 0:0 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	chmod 755 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	exit 10
fi

if [ $OPTION == "busybox" ]; then
	## Install Busybox
	rm -f /system/bin/busybox /system/xbin/busybox
	cp -f $AROMA/busybox /system/xbin/busybox
	chmod 0755 /system/xbin/busybox
	ln -s /system/xbin/busybox /system/bin/busybox
	/system/xbin/busybox --install -s /system/xbin
	exit 10
fi

