#########################
# Copyright (c) 2017-2020 Samsung Electronics, Inc.
# All rights reserved.
#
# This software is the confidential and proprietary information
# of Samsung Electronics, Inc. ("Confidential Information"). You
# shall not disclose such Confidential Information and shall use
# it only in accordance with the terms of the license agreement
# you entered into with Samsung Electronics.
#
# @file   BV_classifier_generic.prototxt
# @brief  prototxt.
################################


name: "MOBILENET/Vizinsight/Generic"
#  transform_param {
#    scale: 0.017
#    mirror: false
#    crop_size: 224
#    mean_value: [103.94,116.78,123.68]
#  }
input: "data"
input_dim: 1
input_dim: 3
input_dim: 192
input_dim: 192
layer {
  name: "insight1"
  type: "Convolution"
  bottom: "data"
  top: "conv1"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 32
    bias_term: true
    pad: 1
    kernel_size: 3
    stride: 2
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu1"
  type: "ReLU"
  bottom: "conv1"
  top: "conv1"
}
layer {
  name: "insight2_1/dw"
  type: "DepthwiseConvolution"
  bottom: "conv1"
  top: "conv2_1/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 32
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 32
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu2_1/dw"
  type: "ReLU"
  bottom: "conv2_1/dw"
  top: "conv2_1/dw"
}
layer {
  name: "insight2_1/sep"
  type: "PointwiseConvolution"
  bottom: "conv2_1/dw"
  top: "conv2_1/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 64
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu2_1/sep"
  type: "ReLU"
  bottom: "conv2_1/sep"
  top: "conv2_1/sep"
}
layer {
  name: "insight2_2/dw"
  type: "DepthwiseConvolution"
  bottom: "conv2_1/sep"
  top: "conv2_2/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 64
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 64
    #engine: CAFFE
    stride: 2
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu2_2/dw"
  type: "ReLU"
  bottom: "conv2_2/dw"
  top: "conv2_2/dw"
}
layer {
  name: "insight2_2/sep"
  type: "PointwiseConvolution"
  bottom: "conv2_2/dw"
  top: "conv2_2/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu2_2/sep"
  type: "ReLU"
  bottom: "conv2_2/sep"
  top: "conv2_2/sep"
}
layer {
  name: "insight3_1/dw"
  type: "DepthwiseConvolution"
  bottom: "conv2_2/sep"
  top: "conv3_1/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 128
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu3_1/dw"
  type: "ReLU"
  bottom: "conv3_1/dw"
  top: "conv3_1/dw"
}
layer {
  name: "insight3_1/sep"
  type: "PointwiseConvolution"
  bottom: "conv3_1/dw"
  top: "conv3_1/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu3_1/sep"
  type: "ReLU"
  bottom: "conv3_1/sep"
  top: "conv3_1/sep"
}
layer {
  name: "insight3_2/dw"
  type: "DepthwiseConvolution"
  bottom: "conv3_1/sep"
  top: "conv3_2/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 128
    #engine: CAFFE
    stride: 2
    weight_filler {
      type: "msra"
    }
  }
}

layer {
  name: "relu3_2/dw"
  type: "ReLU"
  bottom: "conv3_2/dw"
  top: "conv3_2/dw"
}
layer {
  name: "insight3_2/sep"
  type: "PointwiseConvolution"
  bottom: "conv3_2/dw"
  top: "conv3_2/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 256
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu3_2/sep"
  type: "ReLU"
  bottom: "conv3_2/sep"
  top: "conv3_2/sep"
}
layer {
  name: "insight4_1/dw"
  type: "DepthwiseConvolution"
  bottom: "conv3_2/sep"
  top: "conv4_1/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 256
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 256
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu4_1/dw"
  type: "ReLU"
  bottom: "conv4_1/dw"
  top: "conv4_1/dw"
}
layer {
  name: "insight4_1/sep"
  type: "PointwiseConvolution"
  bottom: "conv4_1/dw"
  top: "conv4_1/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 256
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu4_1/sep"
  type: "ReLU"
  bottom: "conv4_1/sep"
  top: "conv4_1/sep"
}
layer {
  name: "insight4_2/dw"
  type: "DepthwiseConvolution"
  bottom: "conv4_1/sep"
  top: "conv4_2/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 256
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 256
    #engine: CAFFE
    stride: 2
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu4_2/dw"
  type: "ReLU"
  bottom: "conv4_2/dw"
  top: "conv4_2/dw"
}
layer {
  name: "insight4_2/sep"
  type: "PointwiseConvolution"
  bottom: "conv4_2/dw"
  top: "conv4_2/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu4_2/sep"
  type: "ReLU"
  bottom: "conv4_2/sep"
  top: "conv4_2/sep"
}
layer {
  name: "insight5_1/dw"
  type: "DepthwiseConvolution"
  bottom: "conv4_2/sep"
  top: "conv5_1/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_1/dw"
  type: "ReLU"
  bottom: "conv5_1/dw"
  top: "conv5_1/dw"
}
layer {
  name: "insight5_1/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_1/dw"
  top: "conv5_1/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_1/sep"
  type: "ReLU"
  bottom: "conv5_1/sep"
  top: "conv5_1/sep"
}
layer {
  name: "insight5_2/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_1/sep"
  top: "conv5_2/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_2/dw"
  type: "ReLU"
  bottom: "conv5_2/dw"
  top: "conv5_2/dw"
}
layer {
  name: "insight5_2/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_2/dw"
  top: "conv5_2/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_2/sep"
  type: "ReLU"
  bottom: "conv5_2/sep"
  top: "conv5_2/sep"
}
layer {
  name: "insight5_3/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_2/sep"
  top: "conv5_3/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_3/dw"
  type: "ReLU"
  bottom: "conv5_3/dw"
  top: "conv5_3/dw"
}
layer {
  name: "insight5_3/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_3/dw"
  top: "conv5_3/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_3/sep"
  type: "ReLU"
  bottom: "conv5_3/sep"
  top: "conv5_3/sep"
}
layer {
  name: "insight5_4/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_3/sep"
  top: "conv5_4/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_4/dw"
  type: "ReLU"
  bottom: "conv5_4/dw"
  top: "conv5_4/dw"
}
layer {
  name: "insight5_4/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_4/dw"
  top: "conv5_4/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_4/sep"
  type: "ReLU"
  bottom: "conv5_4/sep"
  top: "conv5_4/sep"
}
layer {
  name: "insight5_5/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_4/sep"
  top: "conv5_5/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_5/dw"
  type: "ReLU"
  bottom: "conv5_5/dw"
  top: "conv5_5/dw"
}
layer {
  name: "insight5_5/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_5/dw"
  top: "conv5_5/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_5/sep"
  type: "ReLU"
  bottom: "conv5_5/sep"
  top: "conv5_5/sep"
}
layer {
  name: "insight5_6/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_5/sep"
  top: "conv5_6/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 512
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 512
    #engine: CAFFE
    stride: 2
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_6/dw"
  type: "ReLU"
  bottom: "conv5_6/dw"
  top: "conv5_6/dw"
}
layer {
  name: "insight5_6/sep"
  type: "PointwiseConvolution"
  bottom: "conv5_6/dw"
  top: "conv5_6/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 1024
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu5_6/sep"
  type: "ReLU"
  bottom: "conv5_6/sep"
  top: "conv5_6/sep"
}
layer {
  name: "insight6/dw"
  type: "DepthwiseConvolution"
  bottom: "conv5_6/sep"
  top: "conv6/dw"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 1024
    bias_term: true
    pad: 1
    kernel_size: 3
    group: 1024
    #engine: CAFFE
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}
layer {
  name: "relu6/dw"
  type: "ReLU"
  bottom: "conv6/dw"
  top: "conv6/dw"
}
layer {
  name: "insight6/sep"
  type: "PointwiseConvolution"
  bottom: "conv6/dw"
  top: "conv6/sep"
  param {
    lr_mult: 1 #only4conv
    decay_mult: 1
  }
  convolution_param {
    num_output: 1024
    bias_term: true
    pad: 0
    kernel_size: 1
    stride: 1
    weight_filler {
      type: "msra"
    }
  }
}

layer {
  name: "relu6/sep"
  type: "ReLU"
  bottom: "conv6/sep"
  top: "conv6/sep"
}

layer {
  name: "pool6"
  type: "Pooling"
  bottom: "conv6/sep"
  top: "pool6"
  pooling_param {
    pool: AVE
    global_pooling: true
  }
}
layer {
 name: "drop1"
 type: "Dropout"
 bottom: "pool6"
 top: "pool6"
 dropout_param {
     dropout_ratio: 0.2
   }
 }

 
layer {
  name: "vizinsight_generic_676/classifier"
  type: "Convolution"
  bottom: "pool6"
  top: "vizinsight/classifier"
  param {
    lr_mult: 10 #only4conv
    decay_mult: 1
  }
  param {
    lr_mult: 1 #only4conv
    decay_mult: 0
  }
  convolution_param {
    num_output: 676
    kernel_size: 1
    weight_filler {
      type: "xavier"
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}

layer {
  name: "prob"
  type: "Softmax"
  bottom: "vizinsight/classifier"
  top: "prob"
}
